#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------

# i used a lazy cache because its simple and only calculates what it needs to
lazy_cache = {}  # type: dict


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # validating inputs
    assert i > 0
    assert i <= 1000000
    assert j > 0
    assert j <= 1000000
    assert isinstance(i, int)
    assert isinstance(j, int)

    # create variables specifcally for running the algo, so output isnt messed up
    lo = i
    hi = j

    # its possible that j < i, so switch them if necessary
    if i > j:
        lo = j
        hi = i

    # code taken from optimatization given by in class quiz
    if lo < hi // 2 + 1:
        lo = hi // 2 + 1

    max_cycle_length = 1
    # make the range include hi, as python range is exclusive
    hi += 1

    for k in range(lo, hi):
        if k in lazy_cache:
            cycle_length = lazy_cache[k]

        else:  # k is not cached, so run collatz algo until algo reaches a number thats cached
            k2 = k
            cycle_length = 1
            while k2 != 1:
                if k2 % 2 == 0:
                    k2 = k2 // 2
                    cycle_length += 1
                else:  # optimization from class, takes two steps into 1
                    k2 = k2 + (k2 >> 1) + 1
                    cycle_length += 2

                if k2 in lazy_cache:
                    cycle_length += lazy_cache[k2] - 1
                    break
            lazy_cache[k] = cycle_length

        if cycle_length > max_cycle_length:
            max_cycle_length = cycle_length

    assert max_cycle_length > 0
    return max_cycle_length


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
