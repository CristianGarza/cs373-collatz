# CS373: Software Engineering Collatz Repo

* Name: Cristian Garza

* EID: cg42574

* GitLab ID: CristianGarza

* HackerRank ID: cristian_garza

* Git SHA: 70731e5a751d8c4498b32be53e38cdcbe70e1725

* GitLab Pipelines: https://gitlab.com/CristianGarza/cs373-collatz/pipelines

* Estimated completion time: 20 hours

* Actual completion time: 13

* Comments: The setup was much, much more confusing than the project itself.
